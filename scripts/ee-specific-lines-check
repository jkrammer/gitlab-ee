#!/usr/bin/env ruby

require_relative 'ee_specific_check/ee_specific_check'

include EESpecificCheck # rubocop:disable Style/MixinUsage
git_version

base = find_compare_base

current_numstat = updated_diff_numstat(base.ce_base, base.ee_base)
updated_numstat = updated_diff_numstat(base.ce_head, base.ee_head)

offenses = updated_numstat.select do |file, updated_delta|
  current_delta = current_numstat[file]

  more_lines =
    current_delta &&
    updated_delta > current_delta &&
    # Don't complain if we're just adding 1 or 2 more lines, which could be
    # `prepend EE::Module` and a blank line that we want.
    !(current_delta == 0 && updated_delta <= 2)

  more_lines && !WHITELIST.any? { |pattern| Dir.glob(pattern).include?(file) }
end

if offenses.empty?
  say "🎉 All good, congrats! 🎉"
else
  puts

  offenses.each do |(file, delta)|
    puts "* 💥 #{file} has #{delta - current_numstat[file]} more different lines than before! 💥"
  end

  say <<~MESSAGE
    ℹ️ Consider using an EE module to add the features you want.
    ℹ️ See this for detail: https://docs.gitlab.com/ee/development/ee_features.html#ee-features-based-on-ce-features
  MESSAGE
end

remove_remotes

say "ℹ️ For more information on why, see https://gitlab.com/gitlab-org/gitlab-ee/issues/2952"

exit(offenses.size)
